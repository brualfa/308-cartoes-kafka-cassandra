package tech.master.lancamento;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Log {
	
	private String servico = "Serviço Cartão"; 
	private LocalDate data = LocalDate.now();
	private LocalTime time = LocalTime.now();
	private String descricao;
	
	public String getServico() {
		return servico;
	}
	public void setServico(String servico) {
		this.servico = servico;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
